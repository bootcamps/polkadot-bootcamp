#!/bin/bash

rustup target add wasm32-unknown-unknown --toolchain nightly
rustup component add rust-src
cargo install --force --locked cargo-contract --version 2.0.0-rc
cargo contract --help


