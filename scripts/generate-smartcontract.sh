#!/bin/bash

if [ "$1" == "" ]; then
    echo "please write arg for smartcontract name"
    exit 0
fi

cargo contract new flipper
cd $1

cargo test
cargo build

