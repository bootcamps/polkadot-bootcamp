#!/bin/bash

path_node_template="/root/substrate-components/substrate-node-template"


#  purge old chain data alice
$path_node_template/target/release/node-template purge-chain --base-path /tmp/alice --chain local


# create node for using alice account
$path_node_template/target/release/node-template
  --base-path /tmp/alice \
  --chain local \
  --alice \
  --port 30333 \
  --ws-port 9945 \
  --rpc-port 9933 \
  --node-key 0000000000000000000000000000000000000000000000000000000000000001 \
  --telemetry-url "wss://telemetry.polkadot.io/submit/ 0" \
  --validator
