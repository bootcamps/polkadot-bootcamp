# Polkadot Bootcamp Notes - 09-2023

> Polkadot is the first fully-sharded blockchain, i.e. it is a multi-chain environment. Polkadot enables scalability by allowing specialized blockchains to communicate with each other in a secure, trust-free environment.Polkadot is built to connect and secure unique blockchains, whether they be public, permissionless networks, private consortium chains, or other Web3 technologies. It enables an internet where independent blockchains can exchange information under common security guarantees.
> https://wiki.polkadot.network/docs/getting-started

## 1. System Informations

 - Debian 12(bookworm) 5.10.0-22-amd64 #1 SMP Debian 5.10.178-3 (2023-04-22) x86_64 GNU/Linux

### System Sources

 - CPU : 6
 - Memory : 8GiB
 - Disk : 30GiB

## Components

 - substrate-node-template
 - substrate-frontend-template
   - yarn
 - substrate-contract-node(binary)
   - glibc-2.29

## 2. Bootcamp Tasks

 - [learn Rust](https://gitlab.com/queryvs-example/rust-example)
 - Polkadot Basics
 - How to use Substrate
 - Build your Blockchain
   1. Build your Blockchain network with [template](https://github.com/substrate-developer-hub/substrate-node-template)
   1. Start a node for user
   1. How to add trusted node
   1. Learn how to add the node authorization pallet your code 
   1. Monitoring your blockchain metrics
   1. Upgrade a running network
   1. Build custom palet your code
   1. How to build a basic smart contract 
   1. Prepare A Local relay chain


## 3. Using

The project is a document for bootcamp but It has scripts for substrate management.


```bash

scripts/start-node.sh
script/start-frontend.sh

# usable other task scripts look scripts folder

```
alice-bob-nodes.png       frontend-base.png
error-code-changes-2.png  started-node-validate-key-created.png
error-code-changes.png    validation-ok.png
firewall-ports.png

![](pics/frontend-base.png)
![](pics/started-node-validate-key-created.png)
![](pics/validation-ok.png)

### Open the firewall ports

![](pics/firewall-ports.png)
